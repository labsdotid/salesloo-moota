<?php

namespace Salesloo_Moota;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Coupon
 */
class Setting
{

    /**
     * Instance.
     *
     * Holds the coupons instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Register Menu
     * 
     * register system info menu
     */
    public function register_menu($submenu)
    {
        $submenu[] = [
            'page_title' => __('Moota Integration', 'salesloo'),
            'menu_title' => __('Moota Integration', 'salesloo'),
            'capability' => 'manage_options',
            'slug'       => 'salesloo-moota',
            'callback'   => [$this, 'page'],
            'position'   => 99
        ];

        return $submenu;
    }

    public function page()
    {
        $saved = '';

        if (isset($_POST['save']) && isset($_POST['__nonce'])) :
            if (check_admin_referer('salesloo-moota', '__nonce') && wp_verify_nonce($_POST['__nonce'], 'salesloo-moota')) :

                unset($_POST['save']);
                unset($_POST['__nonce']);
                unset($_POST['_wp_http_referer']);

                foreach ($_POST as $key => $value) {
                    \update_option($key, $value);
                }

                \flush_rewrite_rules();

                $saved = '<div id="message" class="updated inline"><p><strong>' . __('Your settings have been saved.', 'salesloo') . '</strong></p></div>';
            endif;
        endif;

        echo '<div class="wrap">';
        echo '<h2>' . __('Moota Integration Settings', 'salesloo') . '</h2>';
        echo $saved;
        echo '<form action="" method="post" enctype="multipart/form-data" style="margin-top:30px">';

        \salesloo_field_text([
            'label'       => __('Moota Secret Token', 'salesloo'),
            'name'        => 'moota_secret_token',
            'description' => '',
            'value'       => get_option('moota_secret_token')
        ]);

        \salesloo_field_heading([
            'label' => __('Moota Webhook', 'salesloo'),
            'description' => get_rest_url(null, 'salesloo-moota/v1/webhook/'),
        ]);

        $banks = [
            'mandiri' => [
                'label' => __('Enable Bank Mandiri', 'salesloo-moota'),
                'url' => admin_url() . 'admin.php?page=salesloo-settings&tab=payment_method&section=moota-mandiri'
            ],
            'bca' => [
                'label' => __('Enable Bank BCA', 'saleloo-moota'),
                'url' => admin_url() . 'admin.php?page=salesloo-settings&tab=payment_method&section=moota-bca'
            ],
            'bca_syariah' => [
                'label' => __('Enable Bank BCA Syariah', 'saleloo-moota'),
                'url' => admin_url() . 'admin.php?page=salesloo-settings&tab=payment_method&section=moota-bca-syariah'
            ],
            'bri' => [
                'label' => __('Enable Bank BRI', 'salesloo-moota'),
                'url' => admin_url() . 'admin.php?page=salesloo-settings&tab=payment_method&section=moota-bri'
            ],
            'bni' => [
                'label' => __('Enable Bank BNI', 'salesloo-moota'),
                'url' => admin_url() . 'admin.php?page=salesloo-settings&tab=payment_method&section=moota-bni'
            ],
            'muamalat' => [
                'label' => __('Enable Bank Muamalat', 'salesloo-moota'),
                'url' => admin_url() . 'admin.php?page=salesloo-settings&tab=payment_method&section=moota-muamalat'
            ]
        ];

        foreach ($banks as $key => $b) {

            $value = \get_option('moota_enable_' . $key);
            $desc = '';

            if ($value) {
                $name = str_replace('Enable', 'Payment Method', $b['label']);
                $link = '<a href="' . $b['url'] . '">this page</a>';
                $desc = sprintf(__('Setup %s on %s', 'salesloo'), $name, $link);
            }
            \salesloo_field_toggle([
                'label'           => $b['label'],
                'name'            => 'moota_enable_' . $key,
                'description'     => $desc,
                'value'   => $value,
            ]);
        }

        \salesloo_field_submit();

        wp_nonce_field('salesloo-moota', '__nonce');
        echo '</form>';
        echo '</div>';
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_filter('salesloo/admin/submenu', [$this, 'register_menu'], 1, 10);
    }
}
