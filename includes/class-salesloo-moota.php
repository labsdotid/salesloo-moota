<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Salesloo_Moota
 * @subpackage Salesloo_Moota/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Salesloo_Moota
{

    /**
     * instance
     */
    public static $instance;

    /**
     * Instance.
     *
     * Ensures only one instance of the plugin class is loaded or can be loaded.
     *
     * @since 1.0.0
     * @access public
     */
    public static function run()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Define the core functionality of the plugin.
     */
    public function __construct()
    {
        $this->load_dependencies();
        Salesloo_Moota\Webhook::init();
        Salesloo_Moota\Setting::init();
        $this->load_payment_classes();
    }

    /**
     * load dependencies
     *
     * @return void
     */
    private function load_dependencies()
    {
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-webhook.php';
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-setting.php';
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-mandiri.php';
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-bca.php';
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-bca-syariah.php';
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-bri.php';
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-bni.php';
        require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-muamalat.php';
    }

    private function load_payment_classes()
    {
        add_filter('salesloo/payment_method/classes', function ($classes) {

            if (get_option('moota_enable_mandiri')) {
                $classes[] = 'Salesloo_Moota\Mandiri';
            }
            if (get_option('moota_enable_bca')) {
                $classes[] = 'Salesloo_Moota\Bca';
            }
            if (get_option('moota_enable_bca_syariah')) {
                $classes[] = 'Salesloo_Moota\Bca_Syariah';
            }
            if (get_option('moota_enable_bri')) {
                $classes[] = 'Salesloo_Moota\Bri';
            }
            if (get_option('moota_enable_bni')) {
                $classes[] = 'Salesloo_Moota\Bni';
            }
            if (get_option('moota_enable_muamalat')) {
                $classes[] = 'Salesloo_Moota\Muamalat';
            }

            return $classes;
        });
    }
}
