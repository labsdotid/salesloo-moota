<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Salesloo_Moota
 * @subpackage Salesloo_Moota/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Salesloo_Moota
 * @subpackage Salesloo_Moota/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Salesloo_Moota_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
