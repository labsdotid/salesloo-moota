<?php

namespace Salesloo_Moota;

use \Salesloo\Models\Invoice;

/**
 * Define rest api moota callback
 *
 * @since      1.0.0
 * @package    Salesloo_Moota
 * @subpackage Salesloo_Moota/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Webhook
{
    private static $_instance;

    /**
     * construction
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'register_rest_api']);
    }

    /**
     * init
     */
    public static function init()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * register rest api
     */
    public function register_rest_api()
    {
        register_rest_route('salesloo-moota/v1', '/webhook', array(
            'methods' => \WP_REST_Server::CREATABLE,
            'callback' => [$this, 'endpoint'],
            'permission_callback' => [$this, 'check_autorize']
        ));
    }

    /**
     * endpoint response
     */
    public function endpoint(\WP_REST_Request $request)
    {

        $notifications = json_decode(file_get_contents("php://input"));
        if (!is_array($notifications)) {
            $notifications = json_decode($notifications);
        }

        if (count((array)$notifications) > 0) {
            foreach ((array)$notifications as $n) {
                $this->process_order($n);
            }
        }
        $response = array(
            'success' => 'Success Mas',
        );

        return new \WP_REST_Response($response, 200);
    }

    public function check_autorize()
    {
        if (isset($_SERVER['HTTP_SIGNATURE'])) {
            $payload = file_get_contents("php://input");
            $signature = hash_hmac('sha256', $payload, get_option('moota_secret_token'));

            if ($_SERVER['HTTP_SIGNATURE'] == $signature) return true;
        }

        return false;
    }

    public function process_order($notification)
    {
        $query = "WHERE total = '$notification->amount' AND payment_method LIKE 'moota-%' AND status IN ('unpaid', 'checking_payment')";
        $invoices = Invoice::query($query)->get();

        if ($invoices->found() > 2) {
        } else {
            foreach ($invoices as $invoice) {
                salesloo_update_invoice_status($invoice->ID, 'completed');
            }
        }
    }
}
