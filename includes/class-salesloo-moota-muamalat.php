<?php

namespace Salesloo_Moota;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

use \Salesloo\Abstracts\Payment_Method;

/**
 * Manual payment methods
 */
class Muamalat extends Payment_Method
{

    /**
     * construction
     */
    public function __construct()
    {
        $this->id              = 'moota-muamalat';
        $this->name            = 'Moota Muamalat';
        $this->icon_id         = SALESLOO_MOOTA_URL . 'assets/img/muamalat.jpg';
        $this->title           = __('Bank Muamalat', 'salesloo-moota');
        $this->description     = __('Pembayaran menggunakan transfer ke Bank muamalat', 'salesloo-moota');
        $this->currency        = 'IDR';
        $this->currency_symbol = 'Rp';
        $this->currency_rate   = '';
        $this->enable          = true;
        $this->unique_number   = true;
        $this->instruction     = __('Transfer pembayaran Anda ke salah satu rekening bank muamalat di bawah ini', 'salesloo-moota');
    }

    /**
     * settings
     */
    public function settings()
    {
        ob_start();

        $accounts = salesloo_get_option('moota_muamalat_account');

?>
        <div class="salesloo-field default">
            <div class="salesloo-field-label">
                <label><?php _e('Bank Accounts') ?></label>
            </div>
            <div class="salesloo-field-input">
                <div class="salesloo-field__number">
                    <div class="salesloo_input_table_wrapper" id="moota_muamalat_accounts">
                        <table class="widefat sortable" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="sort">&nbsp;</th>
                                    <th><?php esc_html_e('Account name', 'salesloo-moota'); ?></th>
                                    <th><?php esc_html_e('Account number', 'salesloo-moota'); ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody class="accounts">
                                <?php
                                $i = -1;
                                if ($accounts) {
                                    foreach ($accounts as $account) {
                                        $i++;

                                        echo '<tr class="account">
										<td class="sort"><span class="dashicons dashicons-menu-alt2" style="cursor:pointer"></span></td>
										<td><input style="width:100%" type="text" value="' . esc_attr(wp_unslash($account['bank_account_name'])) . '" name="moota_muamalat_account[' . esc_attr($i) . '][bank_account_name]" /></td>
										<td><input style="width:100%" type="text" value="' . esc_attr($account['bank_account_number']) . '" name="moota_muamalat_account[' . esc_attr($i) . '][bank_account_number]" /></td>
										<td class="sort" onclick="let el = this.parentNode; el.parentNode.removeChild(el);"><span class="dashicons dashicons-no-alt" style="cursor:pointer"></span></td>
									</tr>';
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="7"><button type="button" class="add button"><?php esc_html_e('+ Add account', 'salesloo-moota'); ?></button> </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <script type="text/javascript">
                        jQuery(function() {

                            jQuery('#moota_muamalat_accounts').on('click', 'button.add', function() {

                                var size = jQuery('#moota_muamalat_accounts').find('tbody .account').length;

                                jQuery('<tr class="account">\
									<td class="sort"><span class="dashicons dashicons-menu-alt2" style="cursor:pointer"></span></td>\
									<td><input style="width:100%" type="text" name="moota_muamalat_account[' + size + '][bank_account_name]" /></td>\
									<td><input style="width:100%" type="text" name="moota_muamalat_account[' + size + '][bank_account_number]" /></td>\
                                    <td class="sort" onclick="let el = this.parentNode; el.parentNode.removeChild(el);"><span class="dashicons dashicons-no-alt" style="cursor:pointer"></span></td>\
								</tr>').appendTo('#moota_muamalat_accounts table tbody');

                                return false;
                            });
                        });
                    </script>

                </div>
            </div>
        </div>
    <?php
        return ob_get_clean();
    }

    /**
     * 
     */
    public function print_action()
    {

        ob_start();

    ?>
        <div class="border-t border-dashed">
            <?php
            $accounts = salesloo_get_option('moota_muamalat_account') ? salesloo_get_option('moota_muamalat_account') : [];

            foreach ((array)$accounts as $account) {
            ?>
                <div class="w-full py-5 border-b border-dashed">
                    <div class="flex items-center justify-center space-x-3">
                        <div class="flex-none">
                            <img src="<?php echo $this->get_icon(); ?>" class="w-32">
                        </div>
                        <div class="flex-grow">
                            <div class="flex">
                                <div class="flex-grow text-lg font-bold text-gray-700 pr-5">
                                    (147) <?php echo sanitize_text_field($account['bank_account_number']); ?>
                                </div>
                                <div class="clipboard flex-none w-14 text-blue-700" @click="$copy('<?php echo sanitize_text_field($account['bank_account_number']); ?>');">
                                    <div class="flex items-center leading-lg px-1 whitespace-no-wrap text-grey-900 text-xs cursor-pointer font-bold">
                                        <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7v8a2 2 0 002 2h6M8 7V5a2 2 0 012-2h4.586a1 1 0 01.707.293l4.414 4.414a1 1 0 01.293.707V15a2 2 0 01-2 2h-2M8 7H6a2 2 0 00-2 2v10a2 2 0 002 2h8a2 2 0 002-2v-2" />
                                        </svg>
                                        <span class="ml-1">Copy</span>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-grow text-sm font-bold text-left">
                                a/n : <?php echo sanitize_text_field($account['bank_account_name']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            } ?>
        </div>

        <form method="post">
            <div class="flex w-full mt-5" x-data="{status: '<?php echo \Salesloo\Invoice::instance()->status; ?>'}">
                <button type="submit" class="flex items-center justify-center focus:outline-none text-white text-sm sm:text-base rounded-lg py-4 w-full transition duration-150 ease-in shadow-md" x-bind:disabled="status != 'unpaid'" :class="{'bg-blue-600 hover:bg-blue-700' : status == 'unpaid', 'bg-gray-300 cursor-not-allowed': status != 'unpaid'}">
                    <span class="font-bold text-base tracking-wider"><?php echo _e('Payment Confirmation', 'salesloo'); ?></span>
                </button>
            </div>
            <?php
            wp_nonce_field('salesloo-invoice', '__nonce');
            ?>
        </form>
<?php

        return ob_get_clean();
    }

    public function handle_action($invoice)
    {
        global $salesloo_data;

        if ('POST' != $_SERVER['REQUEST_METHOD']) return;

        if (!isset($_REQUEST['__nonce'])) return;

        if (wp_verify_nonce($_POST['__nonce'], 'salesloo-invoice')) {

            $updated = salesloo_update_invoice_status($invoice->ID, 'checking_payment');

            if ($updated) {
                $salesloo_data['action_code'] = 'success';
                $salesloo_data['action_message'] = __('Thank you for Your payment. we will check your payment and immediately confirm the status of your purchase', 'salesloo');

                return true;
            } else {
                $salesloo_data['action_code'] = 'warning';
                $salesloo_data['action_message'] = __('error processing confirm payment', 'salesloo');
            }
        }

        return false;
    }
}
