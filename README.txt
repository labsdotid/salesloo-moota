=== Salesloo Moota ===
Contributors: fiqhid24
Tags: payment method, salesloo
Requires at least: 5.0
Tested up to: 5.7.2
Stable tag: 0.0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Salesloo Transfer bank payment method with moota

== Description ==

Transfer Bank payment method with auto confirmation with moota api

== Installation ==

1. Upload `salesloo-moota.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==
= 1.1.0 =

* Add BCA Syariah account

= 1.0.4 =
* Fixed bug on can't process invoice with checking_payment status

= 1.0.3 =
* Add button confirmation on invoice page


== Upgrade Notice ==
