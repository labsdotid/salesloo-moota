<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fiqhidayat.com
 * @since             1.0.0
 * @package           Salesloo_Moota
 *
 * @wordpress-plugin
 * Plugin Name:       Salesloo Moota
 * Plugin URI:        https://www.salesloo.com
 * Description:       Salesloo Transfer bank payment method with moota
 * Version:           1.1.4
 * Author:            Salesloo
 * Author URI:        https://www.salesloo.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       salesloo-moota
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('SALESLOO_MOOTA_VERSION', '1.1.4');
define('SALESLOO_MOOTA_URL', plugin_dir_url(__FILE__));
define('SALESLOO_MOOTA_PATH', plugin_dir_path(__FILE__));
define('SALESLOO_MOOTA_ROOT', __FILE__);

require 'update-checker/plugin-update-checker.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-salesloo-moota-activator.php
 */
function activate_salesloo_moota()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-salesloo-moota-activator.php';
    Salesloo_Moota_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-salesloo-moota-deactivator.php
 */
function deactivate_salesloo_moota()
{
    require_once SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota-deactivator.php';
    Salesloo_Moota_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_salesloo_moota');
register_deactivation_hook(__FILE__, 'deactivate_salesloo_moota');

function salesloo_moota_on_plugins_loaded()
{
    if (did_action('salesloo/loaded')) {

        require SALESLOO_MOOTA_PATH . 'includes/class-salesloo-moota.php';

        Salesloo_Moota::run();
    }
}

add_action('plugins_loaded', 'salesloo_moota_on_plugins_loaded');

add_action('plugins_loaded', 'salesloo_moota_load_plugin_textdomain');

function salesloo_moota_load_plugin_textdomain()
{
    load_plugin_textdomain(
        'salesloo-moota',
        false,
        dirname(plugin_basename(__FILE__)) . '/languages'
    );
}
